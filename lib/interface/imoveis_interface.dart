import '../service/service_entities.dart';

abstract class IImoveisRepository {
  Future<Imoveis> getImoveis(String tipo, String estado, String cidade,
      String bairro, String quartos, String pagina, String imoveisPorPagina);
  Future<ImovelDetail> getImovel(String id);
}
