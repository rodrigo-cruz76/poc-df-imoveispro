
import '../service/service_entities.dart';

abstract class IAuthenticationRepository {
  Future<bool> login(Authentication auth);
}
