import '../service/service_entities.dart';

abstract class IFiltroRepository {
  Future<Tipos> getTipos();
  Future<List<Estado>> getEstados(String tipo);
  Future<List<Cidade>> getCidades(String tipo, String estado);
  Future<List<Bairro>> getBairros(String tipo, String estado, String cidade);
  Future<List<Quarto>> getQuartos(
      String tipo, String estado, String cidade, String bairro);
}
