import '../service/service_entities.dart';

abstract class IDashboardRepository {
  Future<Dashboard> getDashboard();
}
