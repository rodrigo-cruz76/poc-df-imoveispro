import 'package:flutter/foundation.dart';
import '../interface/imoveis_interface.dart';
import '../service/service_entities.dart';
import '../service/services.dart';
import '../util/numbers.dart';

class ImoveisRepository extends IImoveisRepository {
  @override
  Future<Imoveis> getImoveis(
      String tipo,
      String estado,
      String cidade,
      String bairro,
      String quartos,
      String pagina,
      String imoveisPorPagina) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getImoveis(
          tipo, estado, cidade, bairro, quartos, pagina, imoveisPorPagina);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Imóveis');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }

  @override
  Future<ImovelDetail> getImovel(String id) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getImovel(id);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Imóvel');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }
}
