import 'package:flutter/foundation.dart';
import '../interface/dashboard_interface.dart';
import '../service/service_entities.dart';
import '../service/services.dart';
import '../util/numbers.dart';

class DashboardRepository extends IDashboardRepository {
  @override
  Future<Dashboard> getDashboard() async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getDashboard();

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Dashboard');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }
}
