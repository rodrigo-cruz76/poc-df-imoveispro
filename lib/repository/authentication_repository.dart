import 'package:flutter/foundation.dart';
import '../util/numbers.dart';
import '../service/service_entities.dart';
import '../service/services.dart';
import '../interface/authentication_interface.dart';

class AuthenticationRepository extends IAuthenticationRepository {
  @override
  Future<bool> login(Authentication auth) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.login(
        auth,
      );

      final statusCode = response?.statusCode ?? 500;
      return range(200, 300).contains(statusCode);
    } catch (e) {
      if (!kReleaseMode) {
        print(e);
        print('Erro realizando login');
      }

      return false;
    } finally {
      remoteService.dispose();
    }
  }
}
