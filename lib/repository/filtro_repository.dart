import 'package:flutter/foundation.dart';
import '../interface/filtro_interface.dart';
import '../service/service_entities.dart';
import '../service/services.dart';
import '../util/numbers.dart';

class FiltroRepository extends IFiltroRepository {
  @override
  Future<Tipos> getTipos() async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getTipos();

      final statusCode = response?.statusCode ?? 500;
      print(statusCode);
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Tipos');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }

  @override
  Future<List<Bairro>> getBairros(
    String tipo,
    String estado,
    String cidade,
  ) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getBairros(tipo, estado, cidade);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Bairros');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }

  @override
  Future<List<Cidade>> getCidades(String tipo, String estado) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getCidades(tipo, estado);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Cidades');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }

  @override
  Future<List<Estado>> getEstados(String tipo) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response = await remoteService.getEstados(tipo);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Estados');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }

  @override
  Future<List<Quarto>> getQuartos(
      String tipo, String estado, String cidade, String bairro) async {
    final remoteService = RemoteService.create(getChopperClient());
    try {
      final response =
          await remoteService.getQuartos(tipo, estado, cidade, bairro);

      final statusCode = response?.statusCode ?? 500;
      if (range(200, 300).contains(statusCode)) {
        return response.body;
      } else {
        return null;
      }
    } catch (e) {
      if (!kReleaseMode) {
        print('Erro buscando Quartos');
      }

      return null;
    } finally {
      remoteService.dispose();
    }
  }
}
