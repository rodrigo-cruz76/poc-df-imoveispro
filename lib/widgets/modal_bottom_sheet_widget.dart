import 'package:flutter/material.dart';

class ModalBottomSheetWidget extends StatelessWidget {
  const ModalBottomSheetWidget({
    Key key,
    @required this.message,
    this.title,
    this.titleIcon,
    this.padding = const EdgeInsets.all(16.0),
    this.closeIcon,
    this.onClosePressed = _defaultOnClosePressed,
    this.actions,
  })  : assert(message != null),
        super(key: key);

  final EdgeInsetsGeometry padding;
  final Widget title;
  final Widget message;
  final Widget titleIcon;
  final Widget closeIcon;
  final List<Widget> actions;
  final void Function(BuildContext context) onClosePressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding?.add(const EdgeInsets.only(bottom: 64)) ??
          const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 80.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          if (onClosePressed != null)
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () => onClosePressed(context),
                child: closeIcon ??
                    Icon(
                      Icons.close,
                      color: Colors.black87,
                    ),
              ),
            ),
          if (titleIcon != null)
            Container(
              padding: EdgeInsets.only(top: 16, bottom: 24),
              alignment: Alignment.center,
              child: titleIcon,
            ),
          if (title != null)
            Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: DefaultTextStyle(
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Theme.of(context).primaryColor,
                    ),
                child: title,
              ),
            ),
          Padding(
            padding: (actions?.isNotEmpty ?? false)
                ? EdgeInsets.zero
                : const EdgeInsets.only(bottom: 32.0),
            child: DefaultTextStyle(
              style: Theme.of(context).textTheme.subtitle1,
              child: message,
            ),
          ),
          if (actions?.isNotEmpty ?? false)
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Align(
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: actions,
                ),
              ),
            ),
        ],
      ),
    );
  }

  static void _defaultOnClosePressed(BuildContext context) {
    Navigator.of(context).pop();
  }
}
