import 'package:flutter/material.dart';

import '../constants.dart';

class Ripple extends StatelessWidget {
  final double radius;
  final Color color;
  final double left;
  final double bottom;

  const Ripple({
    @required this.radius,
    @required this.color,
    this.left,
    this.bottom,
  })  : assert(radius != null),
        assert(color != null);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    return Positioned(
      left: left != null ? left : screenWidth / 2 - radius,
      bottom: bottom != null ? bottom : screenHeight / 2 - radius,
      child: Container(
        width: 2 * radius,
        height: 2 * radius,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
        ),
      ),
    );
  }
}
