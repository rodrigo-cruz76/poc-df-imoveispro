import 'package:flutter/material.dart';

import '../../constants.dart';
import 'custom_button.dart';
import 'custom_imput_field.dart';
import 'fade_slide_transition.dart';

class LoginForm extends StatefulWidget {
  final Animation<double> animation;
  final Function entrar;
  const LoginForm({
    @required this.animation,
    this.entrar,
  }) : assert(animation != null);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String user = 'poc@timipro.com.br';
  String pass = '123456';

  final FocusNode _userFocus = FocusNode();
  final FocusNode _passFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    var height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    var space = height > 650 ? kSpaceM : kSpaceS;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kPaddingL),
      child: Column(
        children: <Widget>[
          FadeSlideTransition(
            animation: widget.animation,
            additionalOffset: 0.0,
            child: CustomInputField(
              controller: new TextEditingController(text: 'poc@timipro.com.br'),
              label: 'Email',
              prefixIcon: Icons.person,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              focusNode: _userFocus,
              onSubmit: (value) {
                _userFocus.unfocus();
                FocusScope.of(context).requestFocus(_passFocus);
              },
              obscureText: false,
              onChanged: (value) {
                user = value;
              },
            ),
          ),
          SizedBox(height: space),
          FadeSlideTransition(
            animation: widget.animation,
            additionalOffset: space,
            child: CustomInputField(
              controller: new TextEditingController(text: '123456'),
              label: 'Senha',
              prefixIcon: Icons.lock,
              textInputAction: TextInputAction.done,
              focusNode: _passFocus,
              onSubmit: (value) {
                _passFocus.unfocus();
              },
              obscureText: true,
              onChanged: (value) {
                pass = value;
              },
            ),
          ),
          SizedBox(height: space),
          FadeSlideTransition(
            animation: widget.animation,
            additionalOffset: 2 * space,
            child: CustomButton(
              color: kBlue,
              textColor: kWhite,
              text: 'Entrar',
              onPressed: () {
                widget.entrar(user, pass);
              },
            ),
          ),
          SizedBox(height: space),
          FadeSlideTransition(
            animation: widget.animation,
            additionalOffset: 5 * space,
            child: CustomButton(
              color: kWhite,
              textColor: kBlack,
              text: 'Esqueci a senha',
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
