import 'package:flutter/material.dart';

import '../../constants.dart';
import '../logo.dart';
import 'fade_slide_transition.dart';

class Header extends StatelessWidget {
  final Animation<double> animation;

  const Header({
    @required this.animation,
  }) : assert(animation != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 4,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kPaddingL),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Logo(
              color: kDarkBlue,
              size: (MediaQuery.of(context).size.height / 16),
            ),
            const SizedBox(height: kSpaceM),
            FadeSlideTransition(
              animation: animation,
              additionalOffset: 0.0,
              child: Text(
                'Bem-vindo à DF Imóveis',
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: kBlack, fontWeight: FontWeight.w500),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
