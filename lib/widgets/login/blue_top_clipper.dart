import 'package:flutter/material.dart';

class BlueTopClipper extends CustomClipper<Path> {
  final double yOffset;
  final double height;

  const BlueTopClipper({
    @required this.yOffset,
    @required this.height
  }) : assert(yOffset != null);

  @override
  Path getClip(Size size) {
    var path = Path()
      ..lineTo(0.0, (height / 3) + yOffset)
      ..quadraticBezierTo(
        size.width / 2.5,
        (height / 3) + yOffset,
        size.width,
        (height / 5) + yOffset,
      )
      ..lineTo(size.width, 0.0)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
