import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Bloqueia a tela com um overlay modal e uma indicação
/// de trabalho de tempo indeterminado
class ProgressIndicatorOverlay extends StatelessWidget {
  const ProgressIndicatorOverlay({
    Key key,
    this.loading = false,
    this.child,
  }) : super(key: key);

  final bool loading;

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        child,
        if (loading)
          const ModalBarrier(
            dismissible: false,
            color: Colors.black26,
          ),
        if (loading)
          Center(
            child: Theme.of(context).platform == TargetPlatform.iOS
                ? const CupertinoActivityIndicator(
                    animating: true,
                    radius: 20,
                  )
                : const CircularProgressIndicator(),
          ),
      ],
    );
  }
}
