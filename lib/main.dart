import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pocdfimoveis/screens/lista/lista.dart';
import 'package:pocdfimoveis/screens/login/login.dart';
import 'package:pocdfimoveis/screens/onboarding/onboarding.dart';
import 'package:pocdfimoveis/widgets/logo.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'dart:io' show Platform;
import 'constants.dart';

Future<void> main() async {
  if (Platform.isAndroid) {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await Firebase.initializeApp(
      name: 'poc-dfimoveis',
      options: Platform.isIOS || Platform.isMacOS
          ? FirebaseOptions(
              appId: '1:297855924061:ios:c6de2b69b03a5be8',
              apiKey: 'AIzaSyClTSo2BfsbgY-YtTL-weDoIFmnU2vqnlc',
              projectId: 'flutter-firebase-plugins',
              messagingSenderId: '297855924061',
              databaseURL: 'https://poc-dfimoveis.firebaseio.com/',
            )
          : FirebaseOptions(
              appId: '1:577764490893:android:ccb943bf19414614f0acc4',
              apiKey: 'AIzaSyClTSo2BfsbgY-YtTL-weDoIFmnU2vqnlc',
              messagingSenderId: '577764490893',
              projectId: 'poc-dfimoveis',
              databaseURL: 'https://poc-dfimoveis.firebaseio.com/',
            ),
    );
    runApp(App(
      app: app,
    ));
  } else {
    runApp(App(
      app: null,
    ));
  }
}

class App extends StatelessWidget {
  final FirebaseApp app;

  const App({Key key, this.app}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: kDarkBlue,
      debugShowCheckedModeBanner: false,
      title: 'DF Imóveis Pró',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Builder(
        builder: (BuildContext context) {
          return MyHomePage(
            title: 'Splash Screen Flutter',
            app: app,
          );
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final FirebaseApp app;

  MyHomePage({Key key, this.title, this.app}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FirebaseMessaging fb;
  DatabaseReference _tokenRef;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    firebase();
  }

  void firebase() async {
    if (widget.app != null) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      //print('Running on ${androidInfo.model}');  // e.g. "Moto G (4)"

      //IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      //print('Running on ${iosInfo.utsname.machine}');

      fb = new FirebaseMessaging();
      fb.getToken().then((token) {
        print(token);
        DatabaseReference tokensRef =
            FirebaseDatabase.instance.reference().child('tokens').child(token);

        tokensRef.once().then((snapshot) {
          Map<dynamic, dynamic> values = snapshot.value;
          if (values == null) {
            tokensRef.set(androidInfo.model);
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return _introScreen(screenHeight);
  }
}

Widget _introScreen(double screenHeight) {
  return Stack(
    alignment: Alignment.center,
    children: <Widget>[
      SplashScreen(
        seconds: 4,
        gradientBackground: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [kDarkBlue, kLightBlue],
        ),
        navigateAfterSeconds: Login(
          screenHeight: screenHeight,
        ),
        loaderColor: Colors.transparent,
      ),
      Logo(
        color: kWhite,
        size: 48.0,
      )
    ],
  );
}
