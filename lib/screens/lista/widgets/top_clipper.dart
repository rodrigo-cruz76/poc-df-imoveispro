import 'package:flutter/material.dart';

class TopClipper extends CustomClipper<Path> {
  final double yOffset;

  const TopClipper({
    @required this.yOffset,
  }) : assert(yOffset != null);

  @override
  Path getClip(Size size) {
    var path = Path()..lineTo(0.0, this.yOffset);
    var unidade = (size.height / 2) / 15;
    var curva = this.yOffset / unidade;
    path.quadraticBezierTo(
      size.width / 2.0,
      this.yOffset - (25 - curva),
      size.width,
      this.yOffset,
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
