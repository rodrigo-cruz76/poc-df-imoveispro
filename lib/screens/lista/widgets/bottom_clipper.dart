import 'package:flutter/material.dart';

class BottomClipper extends CustomClipper<Path> {
  final double yOffset;

  const BottomClipper({
    @required this.yOffset,
  }) : assert(yOffset != null);
  @override
  Path getClip(Size size) {
    var path = Path()
      ..close()
      ..moveTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, size.height - this.yOffset);
    var unidade = (size.height / 2) / 25;
    var curva = this.yOffset / unidade;

    path.quadraticBezierTo(
      size.width / 2.0,
      size.height - (this.yOffset - (25 - curva)),
      0,
      size.height - this.yOffset,
    );
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
