import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pocdfimoveis/constants.dart';
import 'package:pocdfimoveis/interface/imoveis_interface.dart';
import 'package:pocdfimoveis/repository/imoveis_repository.dart';
import 'package:pocdfimoveis/screens/detail/detail.dart';
import 'package:pocdfimoveis/screens/lista/widgets/bottom_clipper.dart';
import 'package:pocdfimoveis/screens/lista/widgets/top_clipper.dart';
import 'package:pocdfimoveis/service/service_entities.dart';
import 'package:pocdfimoveis/widgets/progress_indicator_overlay.dart';
import 'package:pocdfimoveis/widgets/riple.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:intl/intl.dart';

class Lista extends StatefulWidget {
  final double screenHeight;
  final String tipo;
  final String estado;
  final String cidade;
  final String bairro;
  final String quarto;

  const Lista({
    @required this.screenHeight,
    this.tipo = "1",
    this.estado = '0',
    this.cidade = '0',
    this.bairro = '0',
    this.quarto = '3',
  }) : assert(screenHeight != null);
  @override
  _State createState() => _State();
}

class _State extends State<Lista> with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _topClipperAnimation;
  ScrollController _scrollController = new ScrollController();
  AnimationController _rippleAnimationController;
  Animation<double> _rippleAnimation;
  ImoveisRepository repo;
  List<Imovel> _imoveis = new List();
  bool loading = true;
  int pagina = 0;
  @override
  void initState() {
    super.initState();

    repo = new ImoveisRepository();
    _animationController = AnimationController(
      vsync: this,
      duration: kOpenAnimationDuration,
    );

    var clipperOffsetTween = Tween<double>(
      begin: widget.screenHeight / 2,
      end: 50.0,
    );

    _topClipperAnimation = clipperOffsetTween.animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.0,
          1,
          curve: Curves.easeInOut,
        ),
      ),
    );

    _rippleAnimationController = AnimationController(
      vsync: this,
      duration: kRippleAnimationDuration,
    );

    _rippleAnimation = Tween<double>(
      begin: 0.0,
      end: widget.screenHeight,
    ).animate(CurvedAnimation(
      parent: _rippleAnimationController,
      curve: Curves.easeIn,
    ));

    _fetch().then((value) {
      setState(() {
        loading = false;
        _animationController.forward();
      });
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        pagina++;
        setState(() {
          loading = true;
          _fetch();
        });
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _animationController.dispose();
    _rippleAnimationController.dispose();
    super.dispose();
  }

  Future<Null> _refresh() async {
    pagina = 0;
    _imoveis.clear();
    _fetch();
    return null;
  }

  Future<void> _fetch() async {
    final response = await repo.getImoveis(widget.tipo, widget.estado,
        widget.cidade, widget.bairro, widget.quarto, pagina.toString(), '10');
    if (response != null) {
      setState(() {
        _imoveis.addAll(response.imoveis);
        loading = false;
      });
    }
  }

  Future<void> _gotToDetail(idImovel) async {
    await _rippleAnimationController.forward();
    var retorno = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => Detail(
          screenHeight: widget.screenHeight,
          idImovel: idImovel,
        ),
      ),
    );
    await _rippleAnimationController.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: kDarkBlue,
        title: Text("Meus Imóveis"),
      ),
      body: ProgressIndicatorOverlay(
        loading: loading,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
                color: kGrey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: kPaddingL),
                  child: _imoveis.length > 0
                      ? RefreshIndicator(
                          onRefresh: _refresh,
                          child: ListView.builder(
                            controller: _scrollController,
                            itemCount: _imoveis.length,
                            itemBuilder: (context, index) {
                              var imovel = _imoveis[index];
                              var fomat = new NumberFormat("#,##0.00", "pt_BR");
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                  onTap: () {
                                    _gotToDetail(imovel.id);
                                  },
                                  child: Card(
                                      elevation: 2.0,
                                      borderOnForeground: true,
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: 70,
                                          minHeight: 70,
                                          maxWidth: 150,
                                          maxHeight: 150,
                                        ),
                                        child: Container(
                                          child: Row(
                                            children: [
                                              FadeInImage.memoryNetwork(
                                                placeholder: kTransparentImage,
                                                image: imovel.foto,
                                                height: double.infinity,
                                                fit: BoxFit.cover,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    3,
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Container(
                                                  padding:
                                                      EdgeInsets.all(kPaddingM),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .stretch,
                                                    children: [
                                                      Text(imovel.logradouro),
                                                      Text(
                                                          '${imovel.bairro}-${imovel.cidade}'),
                                                      Text(
                                                        'R\$  ${fomat.format(imovel.valor)}',
                                                        style: TextStyle(
                                                            color: kDarkBlue,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w900),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )),
                                ),
                              );
                            },
                          ),
                        )
                      : Center(
                          child: Text(
                          'Sua pesquisa não retorno registros!',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: kDarkBlue, fontWeight: FontWeight.w600),
                        )),
                )),
            AnimatedBuilder(
              animation: _topClipperAnimation,
              child: Container(
                color: kDarkBlue,
              ),
              builder: (_, Widget child) {
                return ClipPath(
                    clipper: TopClipper(yOffset: _topClipperAnimation.value),
                    child: child);
              },
            ),
            AnimatedBuilder(
              animation: _topClipperAnimation,
              child: Container(
                color: kDarkBlue,
              ),
              builder: (_, Widget child) {
                return ClipPath(
                    clipper: BottomClipper(yOffset: _topClipperAnimation.value),
                    child: child);
              },
            ),
            AnimatedBuilder(
              animation: _rippleAnimation,
              builder: (_, Widget child) {
                return Ripple(
                  color: kDarkBlue,
                  radius: _rippleAnimation.value,
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
