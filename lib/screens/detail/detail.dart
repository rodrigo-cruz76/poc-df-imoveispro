import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pocdfimoveis/repository/imoveis_repository.dart';
import 'package:pocdfimoveis/service/service_entities.dart';
import 'package:pocdfimoveis/widgets/progress_indicator_overlay.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:intl/intl.dart';
import '../../constants.dart';

class Detail extends StatefulWidget {
  final double screenHeight;
  final int idImovel;

  const Detail({Key key, this.screenHeight, this.idImovel}) : super(key: key);
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  ImoveisRepository _repo;
  bool _loading = false;
  @override
  void initState() {
    super.initState();
    _repo = new ImoveisRepository();
  }

  Future<ImovelDetail> _fetchImovel() async {
    setState(() {
      _loading = true;
    });
    var response = _repo.getImovel(widget.idImovel.toString());
    if (response != null) {
      setState(() {
        _loading = false;
      });
      return response;
    } else {
      setState(() {
        _loading = false;
      });
      return new ImovelDetail();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ProgressIndicatorOverlay(
            loading: _loading,
            child: FutureBuilder<ImovelDetail>(
                future: _fetchImovel(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return Container();
                      break;
                    case ConnectionState.done:
                      if (snapshot.hasData) {
                        var fomat = new NumberFormat("#,##0.00", "pt_BR");
                        return CustomScrollView(
                          slivers: [
                            SliverAppBar(
                              expandedHeight: 250.0,
                              floating: false,
                              pinned: true,
                              flexibleSpace: FlexibleSpaceBar(
                                  centerTitle: true,
                                  title: Text(
                                      '${snapshot.data.cidade} ${snapshot.data.bairro}',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                      )),
                                  background: Swiper(
                                    itemCount: snapshot.data.fotos.length,
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            FadeInImage.memoryNetwork(
                                      placeholder: kTransparentImage,
                                      image: snapshot.data.fotos[index].url,
                                      height: double.infinity,
                                      fit: BoxFit.cover,
                                      width:
                                          MediaQuery.of(context).size.width / 3,
                                    ),
                                    autoplay: true,
                                  )),
                            ),
                            SliverToBoxAdapter(
                              child: SizedBox(
                                child: Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Text("Detalhes",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6),
                                        Divider(
                                          color: Colors.grey,
                                          indent: 10,
                                          endIndent: 10,
                                          height: 5,
                                        ),
                                        Container(
                                          height: 16.0,
                                        ),
                                        Wrap(
                                          spacing: 4.0,
                                          direction: Axis.vertical,
                                          children: [
                                            Text(snapshot.data.negocio),
                                            Text(
                                                'R\$  ${fomat.format(double.parse(snapshot.data.valor))}'),
                                            Text(
                                                '${snapshot.data.bairro} ${snapshot.data.cidade}'),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SliverToBoxAdapter(
                              child: SizedBox(
                                child: Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Text("Descrição",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6),
                                        Divider(
                                          color: Colors.grey,
                                          indent: 10,
                                          endIndent: 10,
                                          height: 5,
                                        ),
                                        Container(
                                          height: 16.0,
                                        ),
                                        Wrap(
                                          spacing: 4.0,
                                          direction: Axis.vertical,
                                          children: [
                                            Text(snapshot.data.descricao),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SliverToBoxAdapter(
                              child: SizedBox(
                                child: Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Text("Características",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6),
                                        Divider(
                                          color: Colors.grey,
                                          indent: 10,
                                          endIndent: 10,
                                          height: 5,
                                        ),
                                        Container(
                                          height: 16.0,
                                        ),
                                        Wrap(
                                          spacing: 4.0,
                                          direction: Axis.vertical,
                                          children: [
                                            Text(
                                                'Quartos: ${snapshot.data.quartos}'),
                                            Text(
                                                'Banheiros: ${snapshot.data.banheiro}'),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            // SliverList(
                            //   delegate: SliverChildBuilderDelegate(
                            //       (context, index) => Padding(
                            //             padding: const EdgeInsets.all(8.0),
                            //             child: Container(
                            //               height: 75,
                            //               color: Colors.black12,
                            //             ),
                            //           ),
                            //       childCount: 10),
                            // )
                          ],
                        );
                      } else {
                        return Container();
                      }
                      break;
                    default:
                      return Container();
                      break;
                  }
                })));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
