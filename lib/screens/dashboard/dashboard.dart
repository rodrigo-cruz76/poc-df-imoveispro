import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pocdfimoveis/repository/dashboard_repository.dart';
import 'package:pocdfimoveis/screens/filtro/filtro_view.dart';
import 'package:pocdfimoveis/screens/lista/lista.dart';
import 'package:pocdfimoveis/screens/login/login.dart';
import 'package:pocdfimoveis/service/service_entities.dart';
import 'package:pocdfimoveis/widgets/circleCustomClipper.dart';
import 'package:pocdfimoveis/widgets/logo.dart';
import 'package:pocdfimoveis/widgets/riple.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../constants.dart';

class DashboardView extends StatefulWidget {
  final double screenHeight;

  const DashboardView({
    @required this.screenHeight,
  }) : assert(screenHeight != null);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView>
    with TickerProviderStateMixin {
  AnimationController _rippleAnimationController;
  Animation<double> _rippleAnimation;
  DashboardRepository repo;

  @override
  void dispose() {
    _rippleAnimationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    repo = DashboardRepository();
    _rippleAnimationController = AnimationController(
      vsync: this,
      duration: kRippleAnimationDuration,
    );

    _rippleAnimation = Tween<double>(
      begin: 0.0,
      end: widget.screenHeight,
    ).animate(CurvedAnimation(
      parent: _rippleAnimationController,
      curve: Curves.easeIn,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          color: Theme.of(context).primaryColor,
          height: 60.0,
          backgroundColor: Colors.transparent,
          animationDuration: const Duration(milliseconds: 220),
          animationCurve: Curves.easeOut,
          index: 0,
          onTap: (index) {},
          items: <Widget>[
            Padding(
                padding: EdgeInsets.all(5),
                child: FaIcon(
                  FontAwesomeIcons.accessibleIcon,
                  color: Colors.white,
                )),
            Padding(
                padding: EdgeInsets.all(5),
                child: FaIcon(
                  FontAwesomeIcons.home,
                  color: Colors.white,
                )),
            Padding(
                padding: EdgeInsets.all(5),
                child: FaIcon(
                  FontAwesomeIcons.solidClock,
                  color: Colors.white,
                )),
            Padding(
                padding: EdgeInsets.all(5),
                child: FaIcon(
                  FontAwesomeIcons.gofore,
                  color: Colors.white,
                )),
          ],
        ),
        drawer: Drawer(
          elevation: 2.0,
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                  accountEmail: Text("poc@timipro.com.br"),
                  accountName: Text("Poc Timipro"),
                  currentAccountPicture: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      //image:'https://hmlw3imoveis.blob.core.windows.net/fotos/${imovel.foto}',
                      image:
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTUeA4lVp6bG450UGNgCP9ltpW0VrM_rOt_mg&usqp=CAU")),
              ListTile(
                title: Text('configurações'),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Text('Sair'),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => Login(
                        screenHeight: widget.screenHeight,
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          actions: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: FaIcon(FontAwesomeIcons.bell),
            )
          ],
          elevation: 0.0,
          centerTitle: true,
          title: Logo(size: 30, color: kWhite),
        ),
        body: Center(
          child: FutureBuilder(
              future: _getDataDashBoard(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return CircularProgressIndicator();
                    break;
                  case ConnectionState.done:
                    if (snapshot.data != null) {
                      return Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: StaggeredGridView.countBuilder(
                              crossAxisCount: 4,
                              itemCount: snapshot.data.dados.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  _printItem(index, snapshot.data.dados[index]),
                              staggeredTileBuilder: (int index) =>
                                  new StaggeredTile.count(
                                      2, index.isEven ? 2 : 1),
                              mainAxisSpacing: 16.0,
                              crossAxisSpacing: 16.0,
                            ),
                          ),
                          AnimatedBuilder(
                            animation: _rippleAnimation,
                            builder: (_, Widget child) {
                              return Ripple(
                                color: kDarkBlue,
                                radius: _rippleAnimation.value,
                              );
                            },
                          ),
                        ],
                      );
                    } else {
                      return Text("Erro encontrado");
                    }
                    break;
                  default:
                    return Container();
                    break;
                }
              }),
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {},
            child: Icon(
              Icons.add,
            )));
  }

  Future<Dashboard> _getDataDashBoard() {
    return repo.getDashboard();
  }

  Future<void> _gotToList() async {
    await _rippleAnimationController.forward();
    var retorno = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => FiltroView(
          screenHeight: widget.screenHeight,
        ),
      ),
    );
    await _rippleAnimationController.reverse();
  }

  Widget _printItem(index, Dados dashboard) {
    var quantidade = dashboard.quantidade;
    if (index == 0) {
      return InkWell(
        onTap: () {
          _gotToList();
        },
        child: Container(
            decoration: BoxDecoration(
                color: kBlue,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: FaIcon(
                    FontAwesomeIcons.home,
                    color: kWhite,
                  ),
                ),
                Text(
                  "Imóveis ( $quantidade )",
                  style: TextStyle(color: kWhite),
                )
              ],
            )),
      );
    } else if (index == 1) {
      return Container(
          decoration: BoxDecoration(
              color: kBlack,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: FaIcon(
                  FontAwesomeIcons.users,
                  color: kWhite,
                ),
              ),
              Text(
                "Contatos ( $quantidade )",
                style: TextStyle(color: kWhite),
              )
            ],
          ));
    } else if (index == 2) {
      return Container(
          decoration: BoxDecoration(
              color: kDarkBlue,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: FaIcon(
                  FontAwesomeIcons.clock,
                  color: kWhite,
                ),
              ),
              Text(
                "Visitas ( $quantidade )",
                style: TextStyle(color: kWhite),
              )
            ],
          ));
    } else if (index == 3) {
      return Container(
          decoration: BoxDecoration(
              color: kGrey,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: FaIcon(
                  FontAwesomeIcons.userFriends,
                  color: kBlack,
                ),
              ),
              Text(
                "Clientes ( $quantidade )",
                style: TextStyle(color: kBlack),
              )
            ],
          ));
    } else if (index == 4) {
      return Container(
          decoration: BoxDecoration(
              color: kBlue,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: FaIcon(
                  FontAwesomeIcons.facebookMessenger,
                  color: kWhite,
                ),
              ),
              Text(
                "Mensagens ( $quantidade )",
                style: TextStyle(color: kWhite),
              )
            ],
          ));
    } else if (index == 5) {
      return Container(
          decoration: BoxDecoration(
              color: kDarkBlue,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: FaIcon(
                  FontAwesomeIcons.dochub,
                  color: kWhite,
                ),
              ),
              Text(
                "Propostas ( $quantidade )",
                style: TextStyle(color: kWhite),
              )
            ],
          ));
    } else {}
  }
}
