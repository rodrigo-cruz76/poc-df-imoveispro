import 'package:flutter/material.dart';
import 'package:pocdfimoveis/repository/authentication_repository.dart';
import 'package:pocdfimoveis/screens/dashboard/dashboard.dart';
import 'package:pocdfimoveis/service/service_entities.dart';
import 'package:pocdfimoveis/widgets/login/blue_top_clipper.dart';
import 'package:pocdfimoveis/widgets/login/grey_top_clipper.dart';
import 'package:pocdfimoveis/widgets/login/header.dart';
import 'package:pocdfimoveis/widgets/login/login_form.dart';
import 'package:pocdfimoveis/widgets/login/white_top_clipper.dart';
import 'package:pocdfimoveis/widgets/progress_indicator_overlay.dart';
import 'package:pocdfimoveis/widgets/riple.dart';
import '../../util/dialog.dart';
import '../../constants.dart';

class Login extends StatefulWidget {
  final double screenHeight;

  const Login({
    @required this.screenHeight,
  }) : assert(screenHeight != null);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with TickerProviderStateMixin {
  GlobalKey<ScaffoldState> scaffoldKey;
  AnimationController _animationController;
  AnimationController _rippleAnimationController;
  Animation<double> _headerTextAnimation;
  Animation<double> _formElementAnimation;
  Animation<double> _whiteTopClipperAnimation;
  Animation<double> _blueTopClipperAnimation;
  Animation<double> _greyTopClipperAnimation;
  Animation<double> _rippleAnimation;
  bool _loading = false;
  MessageDialog dialog;
  @override
  void dispose() {
    _animationController.dispose();
    _rippleAnimationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    dialog = new MessageDialog();
    scaffoldKey = GlobalKey<ScaffoldState>();
    _animationController = AnimationController(
      vsync: this,
      duration: kLoginAnimationDuration,
    );

    _rippleAnimationController = AnimationController(
      vsync: this,
      duration: kRippleAnimationDuration,
    );

    _rippleAnimation = Tween<double>(
      begin: 0.0,
      end: widget.screenHeight,
    ).animate(CurvedAnimation(
      parent: _rippleAnimationController,
      curve: Curves.easeIn,
    ));

    var fadeSlideTween = Tween<double>(begin: 0.0, end: 1.0);
    _headerTextAnimation = fadeSlideTween.animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.7,
        1.0,
        curve: Curves.easeInOut,
      ),
    ));
    _formElementAnimation = fadeSlideTween.animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.7,
        1.0,
        curve: Curves.easeInOut,
      ),
    ));

    var clipperOffsetTween = Tween<double>(
      begin: widget.screenHeight,
      end: 0.0,
    );
    _blueTopClipperAnimation = clipperOffsetTween.animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          0.7,
          curve: Curves.easeInOut,
        ),
      ),
    );
    _greyTopClipperAnimation = clipperOffsetTween.animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.0,
          0.5,
          curve: Curves.easeInOut,
        ),
      ),
    );
    _whiteTopClipperAnimation = clipperOffsetTween.animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.7,
          0.9,
          curve: Curves.easeInOut,
        ),
      ),
    );

    _animationController.forward();
  }

  Future<void> _goToDashboard(String user, String pass) async {
    print(user);
    print(pass);
    var auth = AuthenticationRepository();
    setState(() {
      _loading = true;
    });
    var result = await auth.login(Authentication(login: user, senha: pass));
    setState(() {
      _loading = false;
    });
    if (result) {
      await _rippleAnimationController.forward();
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => DashboardView(
            screenHeight: widget.screenHeight,
          ),
        ),
      );
    } else {
      dialog.showMessageDialog(
        scaffoldKey.currentContext,
        title: "Erro",
        message: "Login inválido",
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: ProgressIndicatorOverlay(
        loading: _loading,
        child: Stack(
          alignment: Alignment.center,
          children: [
            SingleChildScrollView(
              child: Column(children: [
                Container(
                  height: MediaQuery.of(context).size.height / 2,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                      AnimatedBuilder(
                        animation: _greyTopClipperAnimation,
                        child: Container(
                          color: kGrey,
                        ),
                        builder: (_, Widget child) {
                          return ClipPath(
                            clipper: GreyTopClipper(
                              yOffset: _greyTopClipperAnimation.value,
                              height: MediaQuery.of(context).size.height,
                            ),
                            child: child,
                          );
                        },
                      ),
                      AnimatedBuilder(
                        animation: _blueTopClipperAnimation,
                        child: Container(
                          color: kDarkBlue,
                        ),
                        builder: (_, Widget child) {
                          return ClipPath(
                            clipper: BlueTopClipper(
                              yOffset: _blueTopClipperAnimation.value,
                              height: MediaQuery.of(context).size.height,
                            ),
                            child: child,
                          );
                        },
                      ),
                      AnimatedBuilder(
                        animation: _whiteTopClipperAnimation,
                        child: Container(
                          color: kWhite,
                        ),
                        builder: (_, Widget child) {
                          return ClipPath(
                            clipper: WhiteTopClipper(
                              yOffset: _whiteTopClipperAnimation.value,
                              height: MediaQuery.of(context).size.height,
                            ),
                            child: child,
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Header(
                              animation: _headerTextAnimation,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 100.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        LoginForm(
                          animation: _formElementAnimation,
                          entrar: _goToDashboard,
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
            AnimatedBuilder(
              animation: _rippleAnimation,
              builder: (_, Widget child) {
                return Ripple(
                  color: kDarkBlue,
                  radius: _rippleAnimation.value,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
