import 'package:flutter/material.dart';
import 'package:pocdfimoveis/repository/filtro_repository.dart';
import 'package:pocdfimoveis/screens/lista/lista.dart';
import 'package:pocdfimoveis/service/service_entities.dart';
import 'package:pocdfimoveis/widgets/progress_indicator_overlay.dart';
import 'package:pocdfimoveis/widgets/riple.dart';

import '../../constants.dart';

class FiltroView extends StatefulWidget {
  final double screenHeight;

  const FiltroView({
    @required this.screenHeight,
  }) : assert(screenHeight != null);
  @override
  _FiltroViewState createState() => _FiltroViewState();
}

class _FiltroViewState extends State<FiltroView> with TickerProviderStateMixin {
  AnimationController _rippleAnimationController;
  Animation<double> _rippleAnimation;

  List<MeuTipo> tipos = <MeuTipo>[];
  List<MeuEstado> estados = <MeuEstado>[];
  List<MeuCidade> cidades = <MeuCidade>[];
  List<MeuBairro> bairros = <MeuBairro>[];
  List<MeuQuarto> quartos = <MeuQuarto>[];

  String _indTipo = '0';
  String _indEstado = "0";
  String _indCidade = '0';
  String _indBairro = '0';
  String _indQuarto = '0';
  String _valueTipo = '0';
  String _valueEstado = "0";
  String _valueCidade = '0';
  String _valueBairro = '0';
  String _valueQuarto = '0';

  bool isLoading = true;

  final filtroRepo = FiltroRepository();

  @override
  void initState() {
    super.initState();
    _rippleAnimationController = AnimationController(
      vsync: this,
      duration: kRippleAnimationDuration,
    );

    _rippleAnimation = Tween<double>(
      begin: 0.0,
      end: widget.screenHeight,
    ).animate(CurvedAnimation(
      parent: _rippleAnimationController,
      curve: Curves.easeIn,
    ));
    _startUI();
  }

  Future<void> _startUI() async {
    await _prepareUI();
    setState(() {
      isLoading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _rippleAnimationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text("Meus Imóveis"),
      ),
      body: _createView(),
    );
  }

  _createView() {
    return ProgressIndicatorOverlay(
      loading: isLoading,
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
                child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _tipos(),
                  _estados(),
                  _cidades(),
                  _bairros(),
                  _quartos(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: RaisedButton(
                            onPressed: () async {
                              await _rippleAnimationController.forward();
                              await Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (_) => Lista(
                                    screenHeight:
                                        MediaQuery.of(context).size.height,
                                    tipo: _valueTipo,
                                    estado: _valueEstado,
                                    cidade: _valueCidade,
                                    bairro: _valueBairro,
                                    quarto: _valueQuarto,
                                  ),
                                ),
                              );
                              await _rippleAnimationController.reverse();
                            },
                            color: Theme.of(context).primaryColor,
                            child: Text("FILTRAR",
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )),
          ),
          AnimatedBuilder(
            animation: _rippleAnimation,
            builder: (_, Widget child) {
              return Ripple(
                color: kDarkBlue,
                radius: _rippleAnimation.value,
              );
            },
          ),
        ],
      ),
    );
  }

  Future<void> _prepareUI() async {
    await _getTipos();
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
    await _getEstados(_valueTipo);
    _valueEstado =
        "${estados.firstWhere((element) => element.id == int.parse(_indEstado)).estado.valor}";
    await _getCidades(_valueTipo, _valueEstado);
    _valueCidade =
        "${cidades.firstWhere((element) => element.id == int.parse(_indCidade)).cidade.valor}";
    await _getBairros(_valueTipo, _valueEstado, _valueCidade);
    _valueBairro =
        "${bairros.firstWhere((element) => element.id == int.parse(_indBairro)).bairro.valor}";
    await _getQuartos(_valueTipo, _valueEstado, _valueCidade, _valueBairro);
    _valueQuarto =
        "${quartos.firstWhere((element) => element.id == int.parse(_indBairro)).quarto.valor}";
    return;
  }

  Future<void> _getTipos() async {
    var result = await filtroRepo.getTipos();
    var ind = 0;
    tipos = <MeuTipo>[];
    result?.tipos?.forEach((element) {
      tipos.add(MeuTipo(ind, element));
      ind++;
    });
    _indTipo = "0";
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
  }

  Future<void> _getEstados(String tipo) async {
    var result = await filtroRepo.getEstados(tipo);
    var ind = 1;
    estados = <MeuEstado>[];
    estados.add(MeuEstado(0, Estado(nome: "Selecione...", valor: -1)));
    result?.forEach((element) {
      estados.add(MeuEstado(ind, element));
      ind++;
    });
    _indEstado = "0";
    _valueEstado =
        "${estados.firstWhere((element) => element.id == int.parse(_indEstado)).estado.valor}";
  }

  Future<void> _getCidades(String tipo, String estado) async {
    var result = await filtroRepo.getCidades(tipo, estado);
    var ind = 1;
    cidades = <MeuCidade>[];
    cidades.add(MeuCidade(0, Cidade(nome: "Selecione...", valor: -1)));
    result?.forEach((element) {
      cidades.add(MeuCidade(ind, element));
      ind++;
    });
    _indCidade = "0";
    _valueCidade =
        "${cidades.firstWhere((element) => element.id == int.parse(_indCidade)).cidade.valor}";
  }

  Future<void> _getBairros(String tipo, String estado, String cidade) async {
    var result = await filtroRepo.getBairros(tipo, estado, cidade);
    var ind = 1;
    bairros = <MeuBairro>[];
    bairros.add(MeuBairro(0, Bairro(nome: "Selecione...", valor: -1)));
    result?.forEach((element) {
      bairros.add(MeuBairro(ind, element));
      ind++;
    });
    _indBairro = "0";
    _valueBairro =
        "${bairros.firstWhere((element) => element.id == int.parse(_indBairro)).bairro.valor}";
  }

  Future<void> _getQuartos(
      String tipo, String estado, String cidade, String bairro) async {
    var result = await filtroRepo.getQuartos(tipo, estado, cidade, bairro);
    var ind = 1;
    quartos = <MeuQuarto>[];
    quartos.add(MeuQuarto(0, Quarto(nome: "Selecione...", valor: -1)));
    result?.forEach((element) {
      quartos.add(MeuQuarto(ind, element));
      ind++;
    });
    _indQuarto = "0";
    _valueQuarto =
        "${quartos.firstWhere((element) => element.id == int.parse(_indQuarto)).quarto.valor}";
  }

  Future<void> _selectTipo() async {
    setState(() {
      isLoading = true;
    });
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
    await _getEstados(_valueTipo);
    await _selectEstado();
    return;
  }

  Future<void> _selectEstado() async {
    if (!isLoading)
      setState(() {
        isLoading = true;
      });
    if (estados.length == 0) {
      setState(() {
        isLoading = false;
      });
      return;
    }
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
    _valueEstado =
        "${estados.firstWhere((element) => element.id == int.parse(_indEstado)).estado.valor}";
    await _getCidades(_valueTipo, _valueEstado);
    await _selectCidade();
    return;
  }

  Future<void> _selectCidade() async {
    if (!isLoading)
      setState(() {
        isLoading = true;
      });
    if (cidades.length == 0) {
      setState(() {
        isLoading = false;
      });
      return;
    }
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
    _valueEstado =
        "${estados.firstWhere((element) => element.id == int.parse(_indEstado)).estado.valor}";
    _valueCidade =
        "${cidades.firstWhere((element) => element.id == int.parse(_indCidade)).cidade.valor}";
    await _getBairros(_valueTipo, _valueEstado, _valueCidade);
    await _selectBairro();
    return;
  }

  Future<void> _selectBairro() async {
    if (!isLoading)
      setState(() {
        isLoading = true;
      });
    _valueTipo =
        "${tipos.firstWhere((element) => element.id == int.parse(_indTipo)).tipo.valor}";
    _valueEstado =
        "${estados.firstWhere((element) => element.id == int.parse(_indEstado)).estado.valor}";
    _valueCidade =
        "${cidades.firstWhere((element) => element.id == int.parse(_indCidade)).cidade.valor}";
    _valueBairro =
        "${bairros.firstWhere((element) => element.id == int.parse(_indBairro)).bairro.valor}";
    await _getQuartos(_valueTipo, _valueEstado, _valueCidade, _valueBairro);
    setState(() {
      isLoading = false;
    });
    return;
  }

  _tipos() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Tipo",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                isDense: true,
              ),
              itemHeight: 49,
              isExpanded: true,
              items: tipos.map((valor) {
                return DropdownMenuItem<String>(
                  value: "${valor.id}",
                  child: Row(children: [
                    Expanded(
                        child: Text(
                      valor.tipo.nome,
                      style: TextStyle(fontSize: 13),
                    )),
                  ]),
                );
              }).toList(),
              value: _indTipo,
              onChanged: (newValue) {
                _indTipo = newValue;
                _selectTipo();
              },
            ),
          ),
        ],
      ),
    );
  }

  _estados() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Estado",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                isDense: true,
              ),
              isExpanded: true,
              items: estados.map((valor) {
                return DropdownMenuItem<String>(
                  value: "${valor.id}",
                  child: Text(
                    valor.estado.nome,
                    style: TextStyle(fontSize: 13),
                  ),
                );
              }).toList(),
              value: _indEstado,
              onChanged: (newValue) {
                _indEstado = newValue;
                _selectEstado();
              },
            ),
          ),
        ],
      ),
    );
  }

  _cidades() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Cidade",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                isDense: true,
              ),
              isExpanded: true,
              items: cidades.map((valor) {
                return DropdownMenuItem<String>(
                  value: "${valor.id}",
                  child: Text(
                    valor.cidade.nome,
                    style: TextStyle(fontSize: 13),
                  ),
                );
              }).toList(),
              value: _indCidade,
              onChanged: (newValue) {
                _indCidade = newValue;
                _selectCidade();
              },
            ),
          ),
        ],
      ),
    );
  }

  _bairros() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Bairro",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                isDense: true,
              ),
              isExpanded: true,
              items: bairros.map((valor) {
                return DropdownMenuItem<String>(
                  value: "${valor.id}",
                  child: Text(
                    valor.bairro.nome,
                    style: TextStyle(fontSize: 13),
                  ),
                );
              }).toList(),
              value: _indBairro,
              onChanged: (newValue) {
                _indBairro = newValue;
                _selectBairro();
              },
            ),
          ),
        ],
      ),
    );
  }

  _quartos() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Quartos",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                isDense: true,
              ),
              isExpanded: true,
              items: quartos.map((valor) {
                return DropdownMenuItem<String>(
                  value: "${valor.id}",
                  child: Text(
                    valor.quarto.nome,
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  ),
                );
              }).toList(),
              value: _indQuarto,
              onChanged: (newValue) {
                setState(() {
                  _indQuarto = newValue;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}

class MeuTipo {
  final int id;
  final Tipo tipo;
  MeuTipo(this.id, this.tipo);
}

class MeuEstado {
  final int id;
  final Estado estado;

  MeuEstado(this.id, this.estado);
}

class MeuCidade {
  final int id;
  final Cidade cidade;
  MeuCidade(this.id, this.cidade);
}

class MeuBairro {
  final int id;
  final Bairro bairro;
  MeuBairro(this.id, this.bairro);
}

class MeuQuarto {
  final int id;
  final Quarto quarto;
  MeuQuarto(this.id, this.quarto);
}
