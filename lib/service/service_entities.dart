import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'service_entities.g.dart';

/// Lista de mapeadores entre classes mapeadas para JSON
/// e callbacks que transformam uma instância de [Map] em
/// uma instância da respectiva classe.
///
/// Para que os serviços remotos possam corretamente converter
/// um http [Response] com um corpo JSON em uma instância
/// das classes declaradas aqui, a classe precisa
/// constar no mapeamento abaixo.
///
/// ex: considere um serviço declarado como abaixo
///
/// ```dart
/// @Get(path: '/messages')
///   Future<Response<List<MessageResponse>>> getMessageList(
///     @Header('Authorization') String credentials,
///     @Header('X-serial-number') String deviceSerialNumber, [
///     @Header('If-Modified-Since') String ifModifiedSince,
/// ]);
/// ```
///
/// Para que o corpo JSON retornado por esse serviço seja convertido
/// para uma instância de [List<MessageResponse>], a classe [MessageResponse]
/// precisa estar mapeada abaixo.
///
/// Esse mapa deve constar apenas classes especializadas. Um serviço mapeado
/// como [Response<dynamic>], [Response<List<dynamic>>] ou [Response<Map<String,dynamic>>]
/// não precisa ser mapeado.
///
///
final serviceEntityParsers =
    Map<Type, dynamic Function(Map<String, dynamic>)>.unmodifiable(
  {
    Authentication: (obj) => Authentication.fromJson(obj),
    Dashboard: (obj) => Dashboard.fromJson(obj),
    Dados: (obj) => Dados.fromJson(obj),
    Tipos: (obj) => Tipos.fromJson(obj),
    Tipo: (obj) => Tipo.fromJson(obj),
    Estado: (obj) => Estado.fromJson(obj),
    Cidade: (obj) => Cidade.fromJson(obj),
    Bairro: (obj) => Bairro.fromJson(obj),
    Quarto: (obj) => Quarto.fromJson(obj),
    Imoveis: (obj) => Imoveis.fromJson(obj),
    Imovel: (obj) => Imovel.fromJson(obj),
    ImovelDetail:(obj) => ImovelDetail.fromJson(obj),
    Foto:(obj) => Foto.fromJson(obj),
    Detalhe:(obj) => Detalhe.fromJson(obj),
    MeuResponse: (obj) => MeuResponse.fromJson(obj),
  },
);

/// Converte [json] em um tipo mapeado em [serviceEntityParsers].
///
/// É um erro chamar esse método se [T] não estiver mapeado
/// em [serviceEntityParsers].
T objectFromJson<T>(Map<String, dynamic> json) {
  for (final entry in serviceEntityParsers.entries) {
    if (entry.key == T) {
      return entry.value(json);
    }
  }

  throw JsonSerializableException(
      'Could not map type ${json?.runtimeType} to $T');
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Authentication {
  @JsonKey(name: 'login')
  String login;
  @JsonKey(name: 'senha')
  String senha;

  Authentication({this.login, this.senha});

  factory Authentication.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationFromJson(json);

  factory Authentication.fromJsonString(String json) =>
      _$AuthenticationFromJson(jsonDecode(json));

  Map<String, dynamic> toJson() => _$AuthenticationToJson(this);
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Dashboard {
  @JsonKey(name: 'dados')
  List<Dados> dados;

  Dashboard({this.dados});
  factory Dashboard.fromJson(Map<String, dynamic> json) =>
      _$DashboardFromJson(json);

  factory Dashboard.fromJsonString(String json) =>
      _$DashboardFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Dados {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'quantidade')
  int quantidade;

  Dados({this.nome, this.quantidade});

  factory Dados.fromJson(Map<String, dynamic> json) => _$DadosFromJson(json);

  factory Dados.fromJsonString(String json) =>
      _$DadosFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Tipos {
  @JsonKey(name: 'tipos')
  List<Tipo> tipos;

  Tipos({this.tipos});
  factory Tipos.fromJson(Map<String, dynamic> json) => _$TiposFromJson(json);

  factory Tipos.fromJsonString(String json) =>
      _$TiposFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Tipo {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'valor')
  int valor;

  Tipo({this.nome, this.valor});
  factory Tipo.fromJson(Map<String, dynamic> json) => _$TipoFromJson(json);

  factory Tipo.fromJsonString(String json) => _$TipoFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Estado {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'valor')
  int valor;

  Estado({this.nome, this.valor});
  factory Estado.fromJson(Map<String, dynamic> json) => _$EstadoFromJson(json);

  factory Estado.fromJsonString(String json) =>
      _$EstadoFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Cidade {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'valor')
  int valor;

  Cidade({this.nome, this.valor});
  factory Cidade.fromJson(Map<String, dynamic> json) => _$CidadeFromJson(json);

  factory Cidade.fromJsonString(String json) =>
      _$CidadeFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Bairro {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'valor')
  int valor;

  Bairro({this.nome, this.valor});
  factory Bairro.fromJson(Map<String, dynamic> json) => _$BairroFromJson(json);

  factory Bairro.fromJsonString(String json) =>
      _$BairroFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Quarto {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'valor')
  int valor;

  Quarto({this.nome, this.valor});
  factory Quarto.fromJson(Map<String, dynamic> json) => _$QuartoFromJson(json);

  factory Quarto.fromJsonString(String json) =>
      _$QuartoFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Imoveis {
  @JsonKey(name: 'imoveis')
  List<Imovel> imoveis;

  Imoveis({this.imoveis});

  factory Imoveis.fromJson(Map<String, dynamic> json) =>
      _$ImoveisFromJson(json);

  factory Imoveis.fromJsonString(String json) =>
      _$ImoveisFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Imovel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'cidade')
  String cidade;
  @JsonKey(name: 'logradouro')
  String logradouro;
  @JsonKey(name: 'bairro')
  String bairro;
  @JsonKey(name: 'regiao')
  String regiao;
  @JsonKey(name: 'quartos')
  int quartos;
  @JsonKey(name: 'negocio')
  String negocio;
  @JsonKey(name: 'banheiros')
  int banheiros;
  @JsonKey(name: 'suites')
  int suites;
  @JsonKey(name: 'valor')
  double valor;
  @JsonKey(name: 'descricao')
  String descricao;
  @JsonKey(name: 'foto')
  String foto;

  Imovel(
      {this.id,
      this.cidade,
      this.logradouro,
      this.bairro,
      this.regiao,
      this.quartos,
      this.negocio,
      this.banheiros,
      this.suites,
      this.valor,
      this.descricao,
      this.foto});
  factory Imovel.fromJson(Map<String, dynamic> json) => _$ImovelFromJson(json);

  factory Imovel.fromJsonString(String json) =>
      _$ImovelFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class ImovelDetail {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'cidade')
  String cidade;
  @JsonKey(name: 'bairro')
  String bairro;
  @JsonKey(name: 'negocio')
  String negocio;
  @JsonKey(name: 'logradourdo')
  String logradourdo;
  @JsonKey(name: 'complemento')
  String complemento;
  @JsonKey(name: 'descricao')
  String descricao;
  @JsonKey(name: 'valor')
  String valor;
  @JsonKey(name: 'valorDoCondominio')
  String valorDoCondominio;
  @JsonKey(name: 'quartos')
  String quartos;
  @JsonKey(name: 'suites')
  String suites;
  @JsonKey(name: 'vagas')
  String vagas;
  @JsonKey(name: 'banheiro')
  String banheiro;
  @JsonKey(name: 'fotos')
  List<Foto> fotos;
  @JsonKey(name: 'detalhes')
  List<Detalhe> detalhes;

  ImovelDetail(
      {this.id,
      this.cidade,
      this.bairro,
      this.negocio,
      this.logradourdo,
      this.complemento,
      this.descricao,
      this.valor,
      this.valorDoCondominio,
      this.quartos,
      this.suites,
      this.vagas,
      this.banheiro,
      this.fotos,
      this.detalhes});
  factory ImovelDetail.fromJson(Map<String, dynamic> json) =>
      _$ImovelDetailFromJson(json);

  factory ImovelDetail.fromJsonString(String json) =>
      _$ImovelDetailFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Foto {
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'descricao')
  String descricao;
  @JsonKey(name: 'idDoImovel')
  String idDoImovel;
  @JsonKey(name: 'url')
  String url;

  Foto({this.nome, this.descricao, this.idDoImovel, this.url});
  factory Foto.fromJson(Map<String, dynamic> json) => _$FotoFromJson(json);

  factory Foto.fromJsonString(String json) => _$FotoFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
class Detalhe {
  @JsonKey(name: 'nome')
  String nome;

  Detalhe({this.nome});
  factory Detalhe.fromJson(Map<String, dynamic> json) =>
      _$DetalheFromJson(json);

  factory Detalhe.fromJsonString(String json) =>
      _$DetalheFromJson(jsonDecode(json));
}

@JsonSerializable(
  createToJson: true,
  includeIfNull: false,
  ignoreUnannotated: true,
)
@DateTimeConverter()
class MeuResponse {
  @JsonKey(name: 'nome')
  final String nome;
  @JsonKey(name: 'registro')
  final DateTime registro;

  MeuResponse(
    this.nome,
    this.registro,
  );
  factory MeuResponse.fromJson(Map<String, dynamic> json) =>
      _$MeuResponseFromJson(json);

  factory MeuResponse.fromJsonString(String json) =>
      _$MeuResponseFromJson(jsonDecode(json));

  // Meu responseAsMeu() {
  //   return Meu(
  //     nome: nome,
  //     numeroRegistro: registro,
  //   );
  // }
}

/// Converson entre [DateTime] e [String] para JSON
class DateTimeConverter implements JsonConverter<DateTime, String> {
  const DateTimeConverter({this.parseThrowsError = true})
      : assert(parseThrowsError != null);

  /// Determina se uma string inválida lança
  /// erro de parse ou apenas returna `null`
  final bool parseThrowsError;

  @override
  DateTime fromJson(String json) {
    if (json == null) {
      return null;
    }

    return parseThrowsError ? DateTime.parse(json) : DateTime.tryParse(json);
  }

  @override
  String toJson(DateTime object) {
    return object?.toIso8601String();
  }
}

class JsonSerializableException implements Exception {
  const JsonSerializableException(this.message);

  final String message;

  @override
  String toString() {
    return message;
  }
}
