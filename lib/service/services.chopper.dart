// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$RemoteService extends RemoteService {
  _$RemoteService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = RemoteService;

  @override
  Future<Response<dynamic>> login(Authentication credentials) {
    final $url = '/api/login';
    final $body = credentials;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<Dashboard>> getDashboard() {
    final $url = '/api/dashboard';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Dashboard, Dashboard>($request);
  }

  @override
  Future<Response<Tipos>> getTipos() {
    final $url = '/api/filtros/tipos';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Tipos, Tipos>($request);
  }

  @override
  Future<Response<List<Estado>>> getEstados(String tipo) {
    final $url = '/api/filtros/estados';
    final $params = <String, dynamic>{'tipo': tipo};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Estado>, Estado>($request);
  }

  @override
  Future<Response<List<Cidade>>> getCidades(String tipo, String estado) {
    final $url = '/api/filtros/cidades';
    final $params = <String, dynamic>{'tipo': tipo, 'estado': estado};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Cidade>, Cidade>($request);
  }

  @override
  Future<Response<List<Bairro>>> getBairros(
      String tipo, String estado, String cidade) {
    final $url = '/api/filtros/bairros';
    final $params = <String, dynamic>{
      'tipo': tipo,
      'estado': estado,
      'cidade': cidade
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Bairro>, Bairro>($request);
  }

  @override
  Future<Response<List<Quarto>>> getQuartos(
      String tipo, String estado, String cidade, String bairro) {
    final $url = '/api/filtros/quartos';
    final $params = <String, dynamic>{
      'tipo': tipo,
      'estado': estado,
      'cidade': cidade,
      'bairro': bairro
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<List<Quarto>, Quarto>($request);
  }

  @override
  Future<Response<Imoveis>> getImoveis(
      String tipo,
      String estado,
      String cidade,
      String bairro,
      String quartos,
      String pagina,
      String imoveisPorPagina) {
    final $url = '/api/imoveis';
    final $params = <String, dynamic>{
      'tipo': tipo,
      'estado': estado,
      'cidade': cidade,
      'bairro': bairro,
      'quartos': quartos,
      'pagina': pagina,
      'imoveisPorPagina': imoveisPorPagina
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<Imoveis, Imoveis>($request);
  }

  @override
  Future<Response<ImovelDetail>> getImovel(String id) {
    final $url = '/api/imoveis/$id';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<ImovelDetail, ImovelDetail>($request);
  }
}
