import 'dart:async';
import 'dart:convert';
import 'package:chopper/chopper.dart';
import 'service_entities.dart';
part 'service_engine.dart';
part 'services.chopper.dart';

@ChopperApi(baseUrl: '/api')
abstract class RemoteService extends ChopperService {
  static RemoteService create([ChopperClient client]) =>
      _$RemoteService(client);

  @Post(
    path: '/login',
  )
  Future<Response> login(@Body() Authentication credentials);

  @Get(path: '/dashboard')
  Future<Response<Dashboard>> getDashboard();

  @Get(path: '/filtros/tipos')
  Future<Response<Tipos>> getTipos();

  @Get(path: '/filtros/estados')
  Future<Response<List<Estado>>> getEstados(@Query("tipo") String tipo);

  @Get(path: '/filtros/cidades')
  Future<Response<List<Cidade>>> getCidades(
    @Query("tipo") String tipo,
    @Query("estado") String estado,
  );

  @Get(path: '/filtros/bairros')
  Future<Response<List<Bairro>>> getBairros(
    @Query("tipo") String tipo,
    @Query("estado") String estado,
    @Query("cidade") String cidade,
  );

  @Get(path: '/filtros/quartos')
  Future<Response<List<Quarto>>> getQuartos(
    @Query("tipo") String tipo,
    @Query("estado") String estado,
    @Query("cidade") String cidade,
    @Query("bairro") String bairro,
  );

  @Get(path: '/imoveis')
  Future<Response<Imoveis>> getImoveis(
    @Query("tipo") String tipo,
    @Query("estado") String estado,
    @Query("cidade") String cidade,
    @Query("bairro") String bairro,
    @Query("quartos") String quartos,
    @Query("pagina") String pagina,
    @Query("imoveisPorPagina") String imoveisPorPagina,
  );
  @Get(path: '/imoveis/{id}')
  Future<Response<ImovelDetail>> getImovel(
    @Path('id') String id,
  );
}
