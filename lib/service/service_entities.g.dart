// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_entities.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Authentication _$AuthenticationFromJson(Map<String, dynamic> json) {
  return Authentication(
    login: json['login'] as String,
    senha: json['senha'] as String,
  );
}

Map<String, dynamic> _$AuthenticationToJson(Authentication instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('login', instance.login);
  writeNotNull('senha', instance.senha);
  return val;
}

Dashboard _$DashboardFromJson(Map<String, dynamic> json) {
  return Dashboard(
    dados: (json['dados'] as List)
        ?.map(
            (e) => e == null ? null : Dados.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DashboardToJson(Dashboard instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('dados', instance.dados);
  return val;
}

Dados _$DadosFromJson(Map<String, dynamic> json) {
  return Dados(
    nome: json['nome'] as String,
    quantidade: json['quantidade'] as int,
  );
}

Map<String, dynamic> _$DadosToJson(Dados instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('quantidade', instance.quantidade);
  return val;
}

Tipos _$TiposFromJson(Map<String, dynamic> json) {
  return Tipos(
    tipos: (json['tipos'] as List)
        ?.map(
            (e) => e == null ? null : Tipo.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$TiposToJson(Tipos instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('tipos', instance.tipos);
  return val;
}

Tipo _$TipoFromJson(Map<String, dynamic> json) {
  return Tipo(
    nome: json['nome'] as String,
    valor: json['valor'] as int,
  );
}

Map<String, dynamic> _$TipoToJson(Tipo instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('valor', instance.valor);
  return val;
}

Estado _$EstadoFromJson(Map<String, dynamic> json) {
  return Estado(
    nome: json['nome'] as String,
    valor: json['valor'] as int,
  );
}

Map<String, dynamic> _$EstadoToJson(Estado instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('valor', instance.valor);
  return val;
}

Cidade _$CidadeFromJson(Map<String, dynamic> json) {
  return Cidade(
    nome: json['nome'] as String,
    valor: json['valor'] as int,
  );
}

Map<String, dynamic> _$CidadeToJson(Cidade instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('valor', instance.valor);
  return val;
}

Bairro _$BairroFromJson(Map<String, dynamic> json) {
  return Bairro(
    nome: json['nome'] as String,
    valor: json['valor'] as int,
  );
}

Map<String, dynamic> _$BairroToJson(Bairro instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('valor', instance.valor);
  return val;
}

Quarto _$QuartoFromJson(Map<String, dynamic> json) {
  return Quarto(
    nome: json['nome'] as String,
    valor: json['valor'] as int,
  );
}

Map<String, dynamic> _$QuartoToJson(Quarto instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('valor', instance.valor);
  return val;
}

Imoveis _$ImoveisFromJson(Map<String, dynamic> json) {
  return Imoveis(
    imoveis: (json['imoveis'] as List)
        ?.map((e) =>
            e == null ? null : Imovel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ImoveisToJson(Imoveis instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('imoveis', instance.imoveis);
  return val;
}

Imovel _$ImovelFromJson(Map<String, dynamic> json) {
  return Imovel(
    id: json['id'] as int,
    cidade: json['cidade'] as String,
    logradouro: json['logradouro'] as String,
    bairro: json['bairro'] as String,
    regiao: json['regiao'] as String,
    quartos: json['quartos'] as int,
    negocio: json['negocio'] as String,
    banheiros: json['banheiros'] as int,
    suites: json['suites'] as int,
    valor: (json['valor'] as num)?.toDouble(),
    descricao: json['descricao'] as String,
    foto: json['foto'] as String,
  );
}

Map<String, dynamic> _$ImovelToJson(Imovel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('cidade', instance.cidade);
  writeNotNull('logradouro', instance.logradouro);
  writeNotNull('bairro', instance.bairro);
  writeNotNull('regiao', instance.regiao);
  writeNotNull('quartos', instance.quartos);
  writeNotNull('negocio', instance.negocio);
  writeNotNull('banheiros', instance.banheiros);
  writeNotNull('suites', instance.suites);
  writeNotNull('valor', instance.valor);
  writeNotNull('descricao', instance.descricao);
  writeNotNull('foto', instance.foto);
  return val;
}

ImovelDetail _$ImovelDetailFromJson(Map<String, dynamic> json) {
  return ImovelDetail(
    id: json['id'] as int,
    cidade: json['cidade'] as String,
    bairro: json['bairro'] as String,
    negocio: json['negocio'] as String,
    logradourdo: json['logradourdo'] as String,
    complemento: json['complemento'] as String,
    descricao: json['descricao'] as String,
    valor: json['valor'] as String,
    valorDoCondominio: json['valorDoCondominio'] as String,
    quartos: json['quartos'] as String,
    suites: json['suites'] as String,
    vagas: json['vagas'] as String,
    banheiro: json['banheiro'] as String,
    fotos: (json['fotos'] as List)
        ?.map(
            (e) => e == null ? null : Foto.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    detalhes: (json['detalhes'] as List)
        ?.map((e) =>
            e == null ? null : Detalhe.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ImovelDetailToJson(ImovelDetail instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('cidade', instance.cidade);
  writeNotNull('bairro', instance.bairro);
  writeNotNull('negocio', instance.negocio);
  writeNotNull('logradourdo', instance.logradourdo);
  writeNotNull('complemento', instance.complemento);
  writeNotNull('descricao', instance.descricao);
  writeNotNull('valor', instance.valor);
  writeNotNull('valorDoCondominio', instance.valorDoCondominio);
  writeNotNull('quartos', instance.quartos);
  writeNotNull('suites', instance.suites);
  writeNotNull('vagas', instance.vagas);
  writeNotNull('banheiro', instance.banheiro);
  writeNotNull('fotos', instance.fotos);
  writeNotNull('detalhes', instance.detalhes);
  return val;
}

Foto _$FotoFromJson(Map<String, dynamic> json) {
  return Foto(
    nome: json['nome'] as String,
    descricao: json['descricao'] as String,
    idDoImovel: json['idDoImovel'] as String,
    url: json['url'] as String,
  );
}

Map<String, dynamic> _$FotoToJson(Foto instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('descricao', instance.descricao);
  writeNotNull('idDoImovel', instance.idDoImovel);
  writeNotNull('url', instance.url);
  return val;
}

Detalhe _$DetalheFromJson(Map<String, dynamic> json) {
  return Detalhe(
    nome: json['nome'] as String,
  );
}

Map<String, dynamic> _$DetalheToJson(Detalhe instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  return val;
}

MeuResponse _$MeuResponseFromJson(Map<String, dynamic> json) {
  return MeuResponse(
    json['nome'] as String,
    const DateTimeConverter().fromJson(json['registro'] as String),
  );
}

Map<String, dynamic> _$MeuResponseToJson(MeuResponse instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('nome', instance.nome);
  writeNotNull('registro', const DateTimeConverter().toJson(instance.registro));
  return val;
}
