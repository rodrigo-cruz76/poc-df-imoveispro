part of 'services.dart';

ChopperClient _chopperClient;
ChopperClient getChopperClient() {
  return _chopperClient ??= ChopperClient(
    baseUrl: "https://painel-poc-apimobile.azurewebsites.net/",
    converter: const _ServiceConverter(),
    services: [
      RemoteService.create(),
    ],
  );
}

void disposeChopperClient() {
  _chopperClient?.dispose();
  _chopperClient = null;
}

class _ServiceConverter extends JsonConverter {
  const _ServiceConverter() : super();

  @override
  Response decodeJson<BodyType, InnerType>(Response response) {
    var contentType = response.headers[contentTypeKey];
    var body = response.body;
    if (contentType != null && contentType.contains(jsonHeaders)) {
      body = utf8.decode(response.bodyBytes);
    }

    body = _tryDecodeJson(body);

    if (isTypeOf<BodyType, Iterable<InnerType>>()) {
      body = body
          .map(
            (element) => (element is Map<String, dynamic> &&
                    !isTypeOf<InnerType, Map<String, dynamic>>())
                ? objectFromJson<InnerType>(element)
                : element,
          )
          .cast<InnerType>()
          .toList();
    } else if (isTypeOf<BodyType, Map<String, InnerType>>()) {
      body = body.cast<String, InnerType>();
    } else if (isTypeOf<BodyType, InnerType>()) {
      if (body is Map<String, dynamic> &&
          !isTypeOf<BodyType, Map<String, dynamic>>()) {
        body = objectFromJson<BodyType>(body);
      }
    }

    if (body is BodyType) {
      return response.copyWith<BodyType>(body: body);
    } else {
      return response.copyWith(body: body);
    }
  }

  dynamic _tryDecodeJson(String data) {
    try {
      return json.decode(data);
    } catch (e) {
      chopperLogger.warning(e);
      return data;
    }
  }
}
