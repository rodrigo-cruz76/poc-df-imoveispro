import 'package:flutter/material.dart';
import 'package:pocdfimoveis/widgets/modal_bottom_sheet_widget.dart';

class MessageDialog {
  Future<void> showMessageDialog(BuildContext context,
      {String title, String message, Widget titleIcon}) {
    return showModalBottomSheet(
      context: context,
      builder: (innerContext) => ModalBottomSheetWidget(
        title: Text(title),
        message: Text(message),
        titleIcon: titleIcon,
        onClosePressed: null,
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(innerContext).pop(),
            child: Text("OK"),
          ),
        ],
      ),
    );
  }
}
