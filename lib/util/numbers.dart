Iterable<T> range<T extends num>(T start, T end, [T step]) sync* {
  assert(start != null);
  assert(end != null);
  assert(start < end);

  var increment = step;
  if (increment == null) {
    try {
      increment = num.parse('1.0');
    } catch (_) {
      increment = num.parse('1');
    }
  }

  assert(increment.toDouble() != 0.0);
  final forward = increment.toDouble() > 0.0;
  var current = forward ? start : end;

  if (forward) {
    while (current < end) {
      yield current;
      current += increment;
    }
  } else {
    while (current > start) {
      yield current;
      current += increment;
    }
  }
}
